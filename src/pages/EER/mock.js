export default {
  "tableCount": 107,
  "groupCount": 14,
  "tables": {
    "Salon": {
      "TableOld": "Salon",
      "RelationTables": ["Service", "BookHour"],
      "BizGroup": [
        {
          "id": "Qm9va2luZw==",
          "name": "Booking"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Service": {
      "TableOld": "Service",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Qm9va2luZw==",
          "name": "Booking"
        },
        {
          "id": "U2VydmljZQ==",
          "name": "Service"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "BookHour": {
      "TableOld": "BookHour",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Qm9va2luZw==",
          "name": "Booking"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": true,
      "Migrated": true
    },
    "Booking_Statistic_Slot": {
      "TableOld": "Booking_Statistic_Slot",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Qm9va2luZw==",
          "name": "Booking"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Booking_Suggest": {
      "TableOld": "Booking_Suggest",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Qm9va2luZw==",
          "name": "Booking"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": true,
      "Migrated": false
    },
    "Booking_Suggest_SalonConfig": {
      "TableOld": "Booking_Suggest_SalonConfig",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Qm9va2luZw==",
          "name": "Booking"
        },
        {
          "id": "U2Fsb24=",
          "name": "Salon"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": true,
      "Migrated": false
    },
    "Booking_Switch_Version": {
      "TableOld": "Booking_Switch_Version",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Qm9va2luZw==",
          "name": "Booking"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Booking_Test": {
      "TableOld": "Booking_Test",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Qm9va2luZw==",
          "name": "Booking"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Customer_Booking_Check": {
      "TableOld": "Customer_Booking_Check",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Qm9va2luZw==",
          "name": "Booking"
        },
        {
          "id": "Q3VzdG9tZXI=",
          "name": "Customer"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "SM_BookingTemp": {
      "TableOld": "SM_BookingTemp",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Qm9va2luZw==",
          "name": "Booking"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "tbl_CusInputBooking": {
      "TableOld": "tbl_CusInputBooking",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Qm9va2luZw==",
          "name": "Booking"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Customer": {
      "TableOld": "Customer",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Qm9va2luZw==",
          "name": "Booking"
        },
        {
          "id": "Q3VzdG9tZXI=",
          "name": "Customer"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Staff": {
      "TableOld": "Staff",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Qm9va2luZw==",
          "name": "Booking"
        },
        {
          "id": "U3RhZmY=",
          "name": "Staff"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Booking": {
      "TableOld": "Booking",
      "RelationTables": [
        "CustomerBookHour"
      ],
      "BizGroup": [
        {
          "id": "Qm9va2luZw==",
          "name": "Booking"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": true,
      "Migrated": true
    },
    "Bill_WaitAtSalon": {
      "TableOld": "Bill_WaitAtSalon",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "QmlsbA==",
          "name": "Bill"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "BillService": {
      "TableOld": "BillService",
      "RelationTables": [
        "CustomerSalonStaffServiceProduct"
      ],
      "BizGroup": [
        {
          "id": "QmlsbA==",
          "name": "Bill"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": true,
      "Migrated": true
    },
    "Customer_HairMode_Bill": {
      "TableOld": "Customer_HairMode_Bill",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "QmlsbA==",
          "name": "Bill"
        },
        {
          "id": "Q3VzdG9tZXI=",
          "name": "Customer"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "SM_BillTemp": {
      "TableOld": "SM_BillTemp",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "QmlsbA==",
          "name": "Bill"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "SM_BillTemp_FlowProduct": {
      "TableOld": "SM_BillTemp_FlowProduct",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "QmlsbA==",
          "name": "Bill"
        },
        {
          "id": "UHJvZHVjdA==",
          "name": "Product"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "SM_BillTemp_FlowService": {
      "TableOld": "SM_BillTemp_FlowService",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "QmlsbA==",
          "name": "Bill"
        },
        {
          "id": "U2VydmljZQ==",
          "name": "Service"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Stylist4Men_BillCutFree": {
      "TableOld": "Stylist4Men_BillCutFree",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "QmlsbA==",
          "name": "Bill"
        },
        {
          "id": "U3R5bGlzdDRNZW4=",
          "name": "Stylist4Men"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Customer_Hair_Skin": {
      "TableOld": "Customer_Hair_Skin",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Q3VzdG9tZXI=",
          "name": "Customer"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Customer_Hair_Skin_History": {
      "TableOld": "Customer_Hair_Skin_History",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Q3VzdG9tZXI=",
          "name": "Customer"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Customer_Rating": {
      "TableOld": "Customer_Rating",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Q3VzdG9tZXI=",
          "name": "Customer"
        },
        {
          "id": "UmF0aW5n",
          "name": "Rating"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Customer_Rating_UuDai": {
      "TableOld": "Customer_Rating_UuDai",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Q3VzdG9tZXI=",
          "name": "Customer"
        },
        {
          "id": "UmF0aW5n",
          "name": "Rating"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Customer_UuDai": {
      "TableOld": "Customer_UuDai",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Q3VzdG9tZXI=",
          "name": "Customer"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "CustomerType": {
      "TableOld": "CustomerType",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Q3VzdG9tZXI=",
          "name": "Customer"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "SpecialCustomer": {
      "TableOld": "SpecialCustomer",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Q3VzdG9tZXI=",
          "name": "Customer"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "SpecialCusDetail": {
      "TableOld": "SpecialCusDetail",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Q3VzdG9tZXI=",
          "name": "Customer"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Stylist4Men_Customer": {
      "TableOld": "Stylist4Men_Customer",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Q3VzdG9tZXI=",
          "name": "Customer"
        },
        {
          "id": "U3R5bGlzdDRNZW4=",
          "name": "Stylist4Men"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Booking_SalonBackup": {
      "TableOld": "Booking_SalonBackup",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U2Fsb24=",
          "name": "Salon"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "KetQuaKinhDoanh_Salon": {
      "TableOld": "KetQuaKinhDoanh_Salon",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U2Fsb24=",
          "name": "Salon"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "QLKho_SalonOrder": {
      "TableOld": "QLKho_SalonOrder",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U2Fsb24=",
          "name": "Salon"
        },
        {
          "id": "SW52ZW50b3J5ICh0P24ga2hvKQpRTEtobw==",
          "name": "Inventory (tồn kho)\nQLKho"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "QLKho_SalonOrder_Flow": {
      "TableOld": "QLKho_SalonOrder_Flow",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U2Fsb24=",
          "name": "Salon"
        },
        {
          "id": "SW52ZW50b3J5ICh0P24ga2hvKQpRTEtobw==",
          "name": "Inventory (tồn kho)\nQLKho"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "QLKho_SalonOrder_Status": {
      "TableOld": "QLKho_SalonOrder_Status",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U2Fsb24=",
          "name": "Salon"
        },
        {
          "id": "SW52ZW50b3J5ICh0P24ga2hvKQpRTEtobw==",
          "name": "Inventory (tồn kho)\nQLKho"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Staff_Salon_History": {
      "TableOld": "Staff_Salon_History",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U2Fsb24=",
          "name": "Salon"
        },
        {
          "id": "U3RhZmY=",
          "name": "Staff"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Tbl_Salon": {
      "TableOld": "Tbl_Salon",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U2Fsb24=",
          "name": "Salon"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": true,
      "Migrated": true
    },
    "TBL_SALON_BK20171127": {
      "TableOld": "TBL_SALON_BK20171127",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U2Fsb24=",
          "name": "Salon"
        }
      ],
      "No": "",
      "NotUse": true,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Api_HairMode_Staff": {
      "TableOld": "Api_HairMode_Staff",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3RhZmY=",
          "name": "Staff"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Api_Staff_Video": {
      "TableOld": "Api_Staff_Video",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3RhZmY=",
          "name": "Staff"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "FlowStaff": {
      "TableOld": "FlowStaff",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3RhZmY=",
          "name": "Staff"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "StaffProcedure": {
      "TableOld": "StaffProcedure",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3RhZmY=",
          "name": "Staff"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Staff_Fluctuations": {
      "TableOld": "Staff_Fluctuations",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3RhZmY=",
          "name": "Staff"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Staff_Mistake": {
      "TableOld": "Staff_Mistake",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3RhZmY=",
          "name": "Staff"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "staff_mistake_bk201712011046": {
      "TableOld": "staff_mistake_bk201712011046",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3RhZmY=",
          "name": "Staff"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Staff_Roll": {
      "TableOld": "Staff_Roll",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3RhZmY=",
          "name": "Staff"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Staff_Type": {
      "TableOld": "Staff_Type",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3RhZmY=",
          "name": "Staff"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": true,
      "Migrated": false
    },
    "StaffAutoCondition": {
      "TableOld": "StaffAutoCondition",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3RhZmY=",
          "name": "Staff"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "StaffAutoLevelLog": {
      "TableOld": "StaffAutoLevelLog",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3RhZmY=",
          "name": "Staff"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "StaffAutoLevelup": {
      "TableOld": "StaffAutoLevelup",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3RhZmY=",
          "name": "Staff"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "StaffErrorSpecailCus": {
      "TableOld": "StaffErrorSpecailCus",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3RhZmY=",
          "name": "Staff"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "SuKien_Team_Staff": {
      "TableOld": "SuKien_Team_Staff",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3RhZmY=",
          "name": "Staff"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Tbl_InformationStaffClub": {
      "TableOld": "Tbl_InformationStaffClub",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3RhZmY=",
          "name": "Staff"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Tbl_Staff_Survey": {
      "TableOld": "Tbl_Staff_Survey",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3RhZmY=",
          "name": "Staff"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Tbl_StaffOfClubInformation": {
      "TableOld": "Tbl_StaffOfClubInformation",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3RhZmY=",
          "name": "Staff"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "WorkTime": {
      "TableOld": "WorkTime",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3RhZmY=",
          "name": "Staff"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "WorkflowFile": {
      "TableOld": "WorkflowFile",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3RhZmY=",
          "name": "Staff"
        }
      ],
      "No": "",
      "NotUse": true,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "FlowService": {
      "TableOld": "FlowService",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U2VydmljZQ==",
          "name": "Service"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": true,
      "Migrated": false
    },
    "Salon_Service": {
      "TableOld": "Salon_Service",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U2VydmljZQ==",
          "name": "Service"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Service_Rating": {
      "TableOld": "Service_Rating",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U2VydmljZQ==",
          "name": "Service"
        },
        {
          "id": "UmF0aW5n",
          "name": "Rating"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Service_Rating_Relationship": {
      "TableOld": "Service_Rating_Relationship",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U2VydmljZQ==",
          "name": "Service"
        },
        {
          "id": "UmF0aW5n",
          "name": "Rating"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "TeamService": {
      "TableOld": "TeamService",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U2VydmljZQ==",
          "name": "Service"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "FlowSalary": {
      "TableOld": "FlowSalary",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U2FsYXJ5",
          "name": "Salary"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "FlowSalary_bk201712011046": {
      "TableOld": "FlowSalary_bk201712011046",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U2FsYXJ5",
          "name": "Salary"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Tbl_Category": {
      "TableOld": "Tbl_Category",
      "RelationTables": [
        "Tbl_Category"
      ],
      "BizGroup": [
        {
          "id": "UHJvZHVjdA==",
          "name": "Product"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "FlowProduct": {
      "TableOld": "FlowProduct",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "UHJvZHVjdA==",
          "name": "Product"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": true,
      "Migrated": false
    },
    "Hair_Attribute_Product": {
      "TableOld": "Hair_Attribute_Product",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "UHJvZHVjdA==",
          "name": "Product"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Product": {
      "TableOld": "Product",
      "RelationTables": [
        "Tbl_Category",
        "FlowProduct",
        "Brands",
        "Product",
        "Tbl_GroupProduct"
      ],
      "BizGroup": [
        {
          "id": "UHJvZHVjdA==",
          "name": "Product"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": true,
      "Migrated": false
    },
    "Skin_Attribute": {
      "TableOld": "Skin_Attribute",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "UHJvZHVjdA==",
          "name": "Product"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Skin_Attribute_Product": {
      "TableOld": "Skin_Attribute_Product",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "UHJvZHVjdA==",
          "name": "Product"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Tbl_GroupProduct": {
      "TableOld": "Tbl_GroupProduct",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "UHJvZHVjdA==",
          "name": "Product"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "PayMethod": {
      "TableOld": "PayMethod",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "UHJvZHVjdA==",
          "name": "Product"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Brands": {
      "TableOld": "Brands",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "UHJvZHVjdA==",
          "name": "Product"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": true
    },
    "Inventory_Data": {
      "TableOld": "Inventory_Data",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "SW52ZW50b3J5ICh0P24ga2hvKQpRTEtobw==",
          "name": "Inventory (tồn kho)\nQLKho"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Inventory_Flow": {
      "TableOld": "Inventory_Flow",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "SW52ZW50b3J5ICh0P24ga2hvKQpRTEtobw==",
          "name": "Inventory (tồn kho)\nQLKho"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Inventory_Flow_HC": {
      "TableOld": "Inventory_Flow_HC",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "SW52ZW50b3J5ICh0P24ga2hvKQpRTEtobw==",
          "name": "Inventory (tồn kho)\nQLKho"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Inventory_HC": {
      "TableOld": "Inventory_HC",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "SW52ZW50b3J5ICh0P24ga2hvKQpRTEtobw==",
          "name": "Inventory (tồn kho)\nQLKho"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Inventory_Import": {
      "TableOld": "Inventory_Import",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "SW52ZW50b3J5ICh0P24ga2hvKQpRTEtobw==",
          "name": "Inventory (tồn kho)\nQLKho"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Rating_ConfigPoint": {
      "TableOld": "Rating_ConfigPoint",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "UmF0aW5n",
          "name": "Rating"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Rating_Temp": {
      "TableOld": "Rating_Temp",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "UmF0aW5n",
          "name": "Rating"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Stylist4Men_Class": {
      "TableOld": "Stylist4Men_Class",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3R5bGlzdDRNZW4=",
          "name": "Stylist4Men"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Stylist4Men_Credits": {
      "TableOld": "Stylist4Men_Credits",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3R5bGlzdDRNZW4=",
          "name": "Stylist4Men"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Stylist4Men_GraduationScore": {
      "TableOld": "Stylist4Men_GraduationScore",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3R5bGlzdDRNZW4=",
          "name": "Stylist4Men"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Stylist4Men_PointClubs": {
      "TableOld": "Stylist4Men_PointClubs",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3R5bGlzdDRNZW4=",
          "name": "Stylist4Men"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Stylist4Men_Student": {
      "TableOld": "Stylist4Men_Student",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3R5bGlzdDRNZW4=",
          "name": "Stylist4Men"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Stylist4Men_StudyPackage": {
      "TableOld": "Stylist4Men_StudyPackage",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3R5bGlzdDRNZW4=",
          "name": "Stylist4Men"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Stylist4Men_Tuition": {
      "TableOld": "Stylist4Men_Tuition",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "U3R5bGlzdDRNZW4=",
          "name": "Stylist4Men"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Tbl_Permission": {
      "TableOld": "Tbl_Permission",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "UGVybWlzc2lvbg==",
          "name": "Permission"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Tbl_Permission_Action": {
      "TableOld": "Tbl_Permission_Action",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "UGVybWlzc2lvbg==",
          "name": "Permission"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Tbl_Permission_DefaultPage": {
      "TableOld": "Tbl_Permission_DefaultPage",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "UGVybWlzc2lvbg==",
          "name": "Permission"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Tbl_Permission_Map": {
      "TableOld": "Tbl_Permission_Map",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "UGVybWlzc2lvbg==",
          "name": "Permission"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "Tbl_Permission_Menu": {
      "TableOld": "Tbl_Permission_Menu",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "UGVybWlzc2lvbg==",
          "name": "Permission"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "TrackingWeb_Data": {
      "TableOld": "TrackingWeb_Data",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "VHJhY2tpbmc=",
          "name": "Tracking"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "TrackingWeb_DataV2": {
      "TableOld": "TrackingWeb_DataV2",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "VHJhY2tpbmc=",
          "name": "Tracking"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "TrackingWeb_Event": {
      "TableOld": "TrackingWeb_Event",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "VHJhY2tpbmc=",
          "name": "Tracking"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "TuyenDung_DanhGia": {
      "TableOld": "TuyenDung_DanhGia",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "VHJhY2tpbmc=",
          "name": "Tracking"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "TuyenDung_NguoiTest": {
      "TableOld": "TuyenDung_NguoiTest",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "VHJhY2tpbmc=",
          "name": "Tracking"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "TuyenDung_Nguon": {
      "TableOld": "TuyenDung_Nguon",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "VHJhY2tpbmc=",
          "name": "Tracking"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "TuyenDung_Skill": {
      "TableOld": "TuyenDung_Skill",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "VHJhY2tpbmc=",
          "name": "Tracking"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "TuyenDung_SkillLevel": {
      "TableOld": "TuyenDung_SkillLevel",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "VHJhY2tpbmc=",
          "name": "Tracking"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "TuyenDung_SkillLevel_Map": {
      "TableOld": "TuyenDung_SkillLevel_Map",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "VHJhY2tpbmc=",
          "name": "Tracking"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "TuyenDung_Status": {
      "TableOld": "TuyenDung_Status",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "VHJhY2tpbmc=",
          "name": "Tracking"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "TuyenDung_UngVien": {
      "TableOld": "TuyenDung_UngVien",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "VHJhY2tpbmc=",
          "name": "Tracking"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": false,
      "NeedMigrateBooking": false,
      "Migrated": false
    },
    "QuanHuyen": {
      "TableOld": "QuanHuyen",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Pz9hIGNoPw==",
          "name": "Địa chỉ"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": true,
      "Migrated": true
    },
    "TinhThanh": {
      "TableOld": "TinhThanh",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Pz9hIGNoPw==",
          "name": "Địa chỉ"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": true,
      "Migrated": true
    },
    "SocialThread": {
      "TableOld": "SocialThread",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Pz9hIGNoPw==",
          "name": "Địa chỉ"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": false,
      "Migrated": true
    },
    "Image": {
      "TableOld": "Image",
      "RelationTables": [],
      "BizGroup": [
        {
          "id": "Pz9hIGNoPw==",
          "name": "Địa chỉ"
        }
      ],
      "No": "",
      "NotUse": false,
      "NeedMigrateSprint_123": true,
      "NeedMigrateBooking": false,
      "Migrated": false
    }
  },
  "groups": {
    "Qm9va2luZw==": "Booking",
    "QmlsbA==": "Bill",
    "Q3VzdG9tZXI=": "Customer",
    "U2Fsb24=": "Salon",
    "U3RhZmY=": "Staff",
    "U2VydmljZQ==": "Service",
    "U2FsYXJ5": "Salary",
    "UHJvZHVjdA==": "Product",
    "SW52ZW50b3J5ICh0P24ga2hvKQpRTEtobw==": "Inventory (tồn kho)\nQLKho",
    "UmF0aW5n": "Rating",
    "U3R5bGlzdDRNZW4=": "Stylist4Men",
    "UGVybWlzc2lvbg==": "Permission",
    "VHJhY2tpbmc=": "Tracking",
    "Pz9hIGNoPw==": "Địa chỉ"
  }
}
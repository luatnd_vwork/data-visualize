import React from 'react'
import { Button, notification } from 'antd';
import go from 'gojs/release/go-debug'
import FireStore from '../../base/db/firestore'
import emitter from '../../base/event/emitter'

import style from './style.module.scss'
//import exportedData from './mock'

const firestore = new FireStore();


const diagramConf = {
  node: {
    bgColor: {
      "default": '#eee',
      NotUse: '#666',
      Migrated: '#0faa00',
    },
    borderColor: {
      "default": '#ccc',
      NeedMigrateSprint_123: '#fda047',
    }
  },
  panelItem: {
    decision: {
      color: {
        false: "#fda047",
        true: "#0a8b00",
      }
    }
  }
};

export default class EER extends React.PureComponent {
  state = {
    spreadSheetData: null,
    collapsed: true,
  }
  
  myDiagram = null;
  
  componentDidMount() {
    console.log("Did mount");
  
    emitter.addListener('onEerNewData', this.updateNewData);
    emitter.addListener('EER_updateCollapseState', this.updateCollapseState);
    
    firestore.subscribeEerDoc();
    
    this.initEERDiagram();
  }
  
  componentWillUnmount() {
    console.log("WillUnmount: TODO");
    this.destroyEERDiagram();
  }
  
  componentDidUpdate(prevProps, prevState) {
    console.log("componentDidUpdate");
    
    const collapsedStateChanged = (prevState.collapsed !== this.state.collapsed);
    if (collapsedStateChanged) {
      
      this.updateEERDiagramModel();
      
    } else {
      // Diagram data changed from server
      
      if (this.myDiagram) {
        // Need confirmation before reload the Diagram
        this.noticeAndConfirm();
      } else {
        this.initEERDiagram();
      }
      
    }
  }
  
  noticeAndConfirm() {
    notification['info']({
      key: "OnNewDataNotice",
      btn: <Button type="primary" size="small" icon="reload" onClick={this.onNewDataNoticeConfirm}>Use data</Button>,
      message: 'New data found',
      description: 'Someone updated the SpreadSheet data a few moment ago, do you wanna show new data on diagram ?',
      duration: 3600,
      className: style.infoNotification,
    });
  }
  onNewDataNoticeConfirm = () => {
    notification.close("OnNewDataNotice");
    this.updateEERDiagramModel();
  }
  
  updateNewData = (data) => {
    this.setState({spreadSheetData: data});
  }
  updateCollapseState = (collapsed) => {
    this.setState({collapsed: collapsed});
  }
  
  initEERDiagram() {
    const {spreadSheetData: exportedData, collapsed} = this.state;
    if (exportedData) {
      console.log("Init EER");
      
      const nodeData = EER.buildGoNodeDataArray(exportedData, collapsed);
      const linkData = EER.buildGoLinkDataArray(exportedData);
      this.initGoDiagram(nodeData, linkData);
    }
  }
  
  updateEERDiagramModel() {
    console.log("updateEERDiagramModel");
    
    const {spreadSheetData: exportedData, collapsed} = this.state;
  
    const nodeData = EER.buildGoNodeDataArray(exportedData, collapsed);
    const linkData = EER.buildGoLinkDataArray(exportedData);
    this.myDiagram.model = new go.GraphLinksModel(nodeData, linkData);
  }
  
  destroyEERDiagram() {
   console.log("destroy EER");
  }
  
  render() {
    console.log("EER render: ");
    
    return (
      <div className={style.eerC}>
        
        <div id="myDiagramDiv" className={style.eerContent}/>
      
      </div>
    );
  }
  
  
  /**
   *
   * @param exportedData
   * @param {boolean} nodeItemCollapsed
   * @returns {[]}
   */
  static buildGoNodeDataArray(exportedData, nodeItemCollapsed) {
    
    //let nestedGroupNodeDataArray = [
    //  {"key":-1, "text":"Operating", "category":"Super"},
    //  {"key":-2, "text":"Drying", "category":"Super", "supers":[ -1 ]},
    //  {"key":-3, "text":"Non Drying", "category":"Super"},
    //  {"key":1, "loc":"100 100", "text":"Starting.End", "supers":[ -2 ]},
    //  {"key":2, "loc":"250 100", "text":"Running", "supers":[ -2 ]},
    //  {"key":3, "loc":"100 200", "text":"Starting.Init", "supers":[ -1,-3 ]},
    //  {"key":4, "loc":"250 200", "text":"Washing", "supers":[ -1,-3 ]},
    //  {"key":5, "loc":"100 300", "text":"Stopped", "supers":[ -3 ]},
    //  {"key":6, "loc":"250 300", "text":"Stopping", "supers":[ -3 ]}
    //];
    
    // create the model for the E-R diagram
    let nodeDataArray = [
      // Tables
      {
        key: "Products",
        group: "B",
        supers:["B"],
        bgColor: '#aaa',
        items: [
          {name: "ProductID", iskey: true, figure: "Decision", color: "green"},
          {name: "ProductName", iskey: false, figure: "Cube1", color: "blue"},
          {name: "SupplierID", iskey: false, figure: "Decision", color: "purple"},
          {name: "CategoryID", iskey: false, figure: "Decision", color: "purple"}
        ]
      },
      {
        key: "Suppliers",
        group: "B",
        supers:["B"],
        items: [
          {name: "SupplierID", iskey: true, figure: "Decision", color: "yellow"},
          {name: "CompanyName", iskey: false, figure: "Cube1", color: "blue"},
          {name: "ContactName", iskey: false, figure: "Cube1", color: "blue"},
          {name: "Address", iskey: false, figure: "Cube1", color: "blue"}
        ]
      },
      {
        key: "Categories",
        group: "A",
        supers:["A", "B"],
        items: [{name: "CategoryID", iskey: true, figure: "Decision", color: "yellow"},
          {name: "CategoryName", iskey: false, figure: "Cube1", color: "blue"},
          {name: "Description", iskey: false, figure: "Cube1", color: "blue"},
          {name: "Picture", iskey: false, figure: "TriangleUp", color: "red"}]
      },
      {
        key: "Order Details",
        group: "A",
        supers:["A"],
        items: [
          {name: "OrderID", iskey: true, figure: "Decision", color: "yellow"},
          {name: "ProductID", iskey: true, figure: "Decision", color: "yellow"},
          {name: "UnitPrice", iskey: false, figure: "MagneticData", color: "green"},
          {name: "Quantity", iskey: false, figure: "MagneticData", color: "green"},
          {name: "Discount", iskey: false, figure: "MagneticData", color: "green"}
        ]
      },
      
      // Groups
      {key: "A", text:"Group A", category: "Super", isGroup: true},
      {key: "B", text:"Group B", category: "Super", isGroup: true},
    ];
  
  
    /**
     * Convert exported data into new data
     */
    if (1) {
      const tables = exportedData.tables;
      const groups = exportedData.groups;
  
      const getGroupName = (name) => "Group: " + name;
  
      nodeDataArray = Object.keys(tables).map((tableName) => {
        const table = tables[tableName];
    
        const color = (decision) => decision ? diagramConf.panelItem.decision.color.true : diagramConf.panelItem.decision.color.false;
        const bgColor = (table) => {
          return table.NotUse
            ? diagramConf.node.bgColor.NotUse
            : table.Migrated
              ? diagramConf.node.bgColor.Migrated
              : !table.NeedMigrateSprint_123
                ? diagramConf.node.bgColor.NeedMigrateSprint_123
                : diagramConf.node.bgColor.default
        }
        const borderColor = (table) => {
          return table.NeedMigrateSprint_123
            ? diagramConf.node.borderColor.NeedMigrateSprint_123
            : diagramConf.node.borderColor.default
        }
        
        let node = {
          key: tableName,
          group: table.BizGroup.length ? getGroupName(table.BizGroup[0].name): null,
          
          showItems: !nodeItemCollapsed,
          items: [
            {name: `NotUse: ${table.NotUse ? "TRUE" : "FALSE"}`, figure: "Decision", color: color(table.NotUse)},
            {name: `Sprint_123: ${table.NeedMigrateSprint_123 ? "TRUE" : "FALSE"}`, figure: "Decision", color: color(table.NeedMigrateSprint_123)},
            {name: `Booking: ${table.NeedMigrateBooking ? "TRUE" : "FALSE"}`, figure: "Decision", color: color(table.NeedMigrateBooking)},
            {name: `Migrated: ${table.Migrated ? "TRUE" : "FALSE"}`, figure: "Decision", color: color(table.Migrated)},
          ],
          
          bgColor: bgColor(table),
          borderColor: borderColor(table),
        };
    
        return node;
      });
  
      const groupNodes = Object.keys(groups).map((groupId) => {
        const groupName = groups[groupId];
  
        return {
          key: getGroupName(groupName),
          // text:"Group A",
          //category: "Super", // Trt to Support multi groups
          isGroup: true
        };
      });
      
      
      const noteNode = {
        key: "NOTE",
        //group: "",
        bgColor: 'yellow',
        borderColor: 'orange',
        showItems: true,
        items: [
          {name: `Color meaning:`, figure: "Decision", color: 'transparent'},
          {name: `is "Migrated" table's background color`, figure: "Decision", color: diagramConf.node.bgColor.Migrated},
          {name: `is "NotUse" table's background color`, figure: "Decision", color: diagramConf.node.bgColor.NotUse},
          {name: `is "NeedMigrateSprint_123" table's border color`, figure: "Decision", color: diagramConf.node.borderColor.NeedMigrateSprint_123},
        ],
      };
      
      
      nodeDataArray = [noteNode].concat(nodeDataArray, groupNodes);
    }
    
    return nodeDataArray;
  }
  
  static buildGoLinkDataArray(exportedData) {
    let linkDataArray = [
      {from: "Products", to: "Suppliers", text: "0..N", toText: "1"},
      {from: "Products", to: "Categories", text: "0..N", toText: "1"},
      {from: "Order Details", to: "Products", text: "0..N", toText: "1"}
    ];
  
    if (1) {
      const tables = exportedData.tables;
  
      linkDataArray = [];
      Object.keys(tables).map((tableName) => {
        const table = tables[tableName];
        const toTables = table.RelationTables;
    
        toTables.map(tableName => {
          linkDataArray.push({
            from: table.TableOld,
            to: tableName,
            //text: "0..N", toText: "1"
          });
      
          return true;
        });
    
        return true;
      });
    }

    return linkDataArray;
  };
  
  /**
   * https://gojs.net/latest/samples/entityRelationship.html
   */
  initGoDiagram(nodeDataArray, linkDataArray) {
    var $ = go.GraphObject.make;  // for conciseness in defining templates
    
    var myDiagram =
      $(go.Diagram, "myDiagramDiv",  // must name or refer to the DIV HTML element
        {
          initialContentAlignment: go.Spot.Center,
          allowDelete: false,
          allowCopy: false,
          layout: $(go.ForceDirectedLayout),
  
          /**
           * Nested layout group, multi group suport
           * NOTE: Currently, gojs don't support multi-groups, this is a trick
           */
          //draggingTool: $(CustomDraggingTool),
          //layout: $(CustomLayout),
          // End custom multi group support
          
          
          "undoManager.isEnabled": true
        });
    
    
    /**
     * Luatnd: Save instance
     */
    this.myDiagram = myDiagram;
    /**
     * Luatnd: End Save instance
     */
      
      
      // the template for each attribute in a node's array of item data
    var itemTempl = $(go.Panel, "Horizontal",
      {
        margin: new go.Margin(5, 0, 0, 0),
      },
      $(go.Shape, {
          desiredSize: new go.Size(14, 14),
          stroke: 'transparent',
        },
        new go.Binding("figure", "figure"),
        new go.Binding("fill", "color")
      ),
      $(go.TextBlock, {
          margin: new go.Margin(0, 0, 0, 10),
          stroke: "#333333",
          font: "normal 14px sans-serif",
        },
        new go.Binding("text", "name"))
      );
    
    
    // define the Node template, representing an entity
    myDiagram.nodeTemplate = $(
      go.Node, "Auto",  // the whole node panel
      {
        selectionAdorned: true,
        resizable: true,
        layoutConditions: go.Part.LayoutStandard & ~go.Part.LayoutNodeSized,
        fromSpot: go.Spot.AllSides,
        toSpot: go.Spot.AllSides,
        isShadowed: true,
        shadowColor: "rgba(158,158,158,0.5)", // Node Shadow color
      },
      
      new go.Binding("location", "location").makeTwoWay(),
      
      
      // whenever the PanelExpanderButton changes the visible property of the "LIST" panel,
      // clear out any desiredSize set by the ResizingTool.
      new go.Binding("desiredSize", "visible", function (v) {
        return new go.Size(NaN, NaN);
      }).ofObject("LIST"),
      
      
      // define the node's outer shape, which will surround the Table
      $(go.Shape, "Rectangle",
        {
          fill: '#eee',
          stroke: "#aaa",
          strokeWidth: 2,
        },
        new go.Binding("fill", "bgColor"),
        new go.Binding("stroke", "borderColor"),
      ),
      
      
      $(go.Panel, "Table",
        
        {margin: 8, stretch: go.GraphObject.Fill},
        
        $(go.RowColumnDefinition, {row: 0, sizing: go.RowColumnDefinition.None}),
        
        
        // the table header
        $(go.TextBlock,
          {
            row: 0, alignment: go.Spot.Center,
            margin: new go.Margin(0, 14, 0, 2),  // leave room for Button
            font: "bold 16px sans-serif"
          },
          new go.Binding("text", "key")),
        
        
        // the collapse/expand button
        $("PanelExpanderButton", "LIST",  // the name of the element whose visibility this button toggles
          {row: 0, alignment: go.Spot.TopRight}),
        
        
        // the list of Panels, each showing an attribute
        $(go.Panel, "Vertical",
          {
            name: "LIST",
            row: 1,
            padding: 3,
            alignment: go.Spot.TopLeft,
            defaultAlignment: go.Spot.Left,
            stretch: go.GraphObject.Horizontal,
            itemTemplate: itemTempl,
          },
          new go.Binding("itemArray", "items"),
          new go.Binding("visible", "showItems"),
        ),
      )  // end Table Panel
    
    
    );  // end Node template
  
    // define the group template
    myDiagram.groupTemplate =
      $(go.Group, "Spot",
        {
          // adornment when a group is selected
          selectionAdornmentTemplate: $(go.Adornment, "Auto",
            $(go.Shape, "Rectangle", {fill: null, stroke: "dodgerblue", strokeWidth: 3}),
            $(go.Placeholder)
          ),
          toSpot: go.Spot.AllSides, // links coming into groups at any side
          toEndSegmentLength: 30, fromEndSegmentLength: 30
        },
        $(go.Panel, "Auto",
          $(go.Shape, "Rectangle",
            {
              name: "OBJSHAPE",
              parameter1: 14,
              fill: 'rgba(136,187,233,0.1)', // group background
              stroke: "lightgray", // group border
              strokeWidth: 1,
            },
            new go.Binding("desiredSize", "ds")),
          $(go.Placeholder, {padding: 16})
        ),
        $(go.TextBlock,
          {
            name: "GROUPTEXT",
            alignment: go.Spot.TopLeft,
            alignmentFocus: new go.Spot(0, 0, 10, -30),
            font: "Bold 20pt Sans-Serif",
          },
          new go.Binding("text", "key"),
        ),
        {
          toolTip:  //  define a tooltip for each group that displays its information
            $(go.Adornment, "Auto",
              $(go.Shape, {fill: "#EFEFCC"}),
              $(go.TextBlock, {margin: 4},
                new go.Binding("text", "", getInfo))
            )
        }
      );
    // end define the group template
    
  
    // START multi grouptemplate: Support ulti groups trick
    //myDiagram.nodeTemplateMap.add("Super",
    //  $(go.Node, "Auto",
    //    { locationObjectName: "BODY" },
    //    $(go.Shape, "RoundedRectangle",
    //      {
    //        fill: "rgba(128, 128, 64, 0.5)", strokeWidth: 1.5, parameter1: 20,
    //        spot1: go.Spot.TopLeft, spot2: go.Spot.BottomRight, minSize: new go.Size(30, 30)
    //      }),
    //    $(go.Panel, "Vertical",
    //      { margin: 10 },
    //      $(go.TextBlock,
    //        { font: "bold 10pt sans-serif", margin: new go.Margin(0, 0, 5, 0) },
    //        new go.Binding("text")),
    //      $(go.Shape,
    //        { name: "BODY", opacity: 0 })
    //    )
    //  ));
    // End nested grouptemplate
    
    // define the Link template, representing a relationship
    myDiagram.linkTemplate =
      $(go.Link,  // the whole link panel
        {
          routing: go.Link.Normal,
          //routing: go.Link.Orthogonal,
          //routing: go.Link.AvoidsNodes,
          corner: 100,
  
          smoothness: 1.0, // how far the control points are from the points of the route when routing is Orthogonal and curve is Bezier.
          
          curve: go.Link.None,
          //curve: go.Link.Bezier,
          //curve: go.Link.JumpOver,
          curviness: 30, // For curve.Bezier
          
          toShortLength: 2
        },
        // the link shape
        $(go.Shape, {stroke: "gray", strokeWidth: 2}),
        // the "from" arrowhead
        $(go.Shape, {toArrow: "Standard"}),
        // the "to" arrowhead
        $(go.Shape, {toArrow: "Standard", scale: 1.5, fill: "black"}),
        
        {
          click: (e, obj) => {
            var part = obj.part;
            if (part instanceof go.Adornment) part = part.adornedPart;
            
            var data = null;
            if (part instanceof go.Link) {
              var link = part;
              data = link.data;
            } else if (part instanceof go.Node) {
              var node = part;
              var link = node.linksConnected.first();
              data = link.data;
            }
            
            console.log("Clicked on link: ", data);
          },
          toolTip:  // define a tooltip for each link that displays its information
            $(go.Adornment, "Auto",
              $(go.Shape, {fill: "#EFEFCC"}),
              $(go.TextBlock, {margin: 4},
                new go.Binding("text", "", infoString).ofObject())
            )
        }
      );
    
    
    // Do render graph base on input data and defined template
    myDiagram.model = new go.GraphLinksModel(nodeDataArray, linkDataArray);
  }
}





function nodesTo(x, i) {
  var nodesToList = new go.List("string");
  if (x instanceof go.Link) {
    x.fromNode.highlight = i;
    nodesToList.add(x.data.from);
  } else {
    x.findNodesInto().each(function (node) {
      node.highlight = i;
      nodesToList.add(node.data.key);
    });
  }
  return nodesToList;
}

// same as nodesTo, but 'from' instead of 'to'
function nodesFrom(x, i) {
  var nodesFromList = new go.List("string");
  if (x instanceof go.Link) {
    x.toNode.highlight = i;
    nodesFromList.add(x.data.to);
  } else {
    x.findNodesOutOf().each(function (node) {
      node.highlight = i;
      nodesFromList.add(node.data.key);
    });
  }
  return nodesFromList;
}

// If x is a link, highlight its toNode, or if it is a node, the node(s) it links to,
// and then call nodesReach on the highlighted node(s), with the next color.
// Do not highlight any node that has already been highlit with a color
// indicating a closer relationship to the original node.
function nodesReach(x, i) {
  if (x instanceof go.Link) {
    x.toNode.highlight = i;
    nodesReach(x.toNode, i + 1);
  } else {
    x.findNodesOutOf().each(function (node) {
      if (node.highlight === 0 || node.highlight > i) {
        node.highlight = i;
        nodesReach(node, i + 1);
      }
    });
  }
}

// highlight all nodes linked to this one
function nodesConnect(x, i) {
  if (x instanceof go.Link) {
    x.toNode.highlight = i;
    x.fromNode.highlight = i;
  } else {
    x.findNodesConnected().each(function (node) {
      node.highlight = i;
    });
  }
}

// highlights the group containing this object, specific method for links
// returns the containing group of x
function containing(x, i) {
  var container = x.containingGroup;
  if (container !== null) container.highlight = i;
  return container;
}

// container is the group that contains this node and
// will be the parameter x for the next call of this function.
// Calling containing(x,i) highlights each group the appropriate color
function containingAll(x, i) {
  containing(x, i);
  var container = x.containingGroup;
  if (container !== null) containingAll(container, i + 1);
}

// if the Node"s containingGroup is x, highlight it
function childNodes(x, i) {
  var myDiagram = window.myDiagram;
  
  var childLst = new go.List("string");
  if (x instanceof go.Group) {
    myDiagram.nodes.each(function (node) {
      if (node.containingGroup === x) {
        node.highlight = i;
        childLst.add(node.data.key);
      }
    });
  }
  return childLst;
}

// same as childNodes, then run allMemberNodes for each child Group with the next color
function allMemberNodes(x, i) {
  var myDiagram = window.myDiagram;
  
  if (x instanceof go.Group) {
    myDiagram.nodes.each(function (node) {
      if (node.containingGroup === x) {
        node.highlight = i;
        allMemberNodes(node, i + 1);
      }
    });
  }
}

// if the link"s containing Group is x, highlight it
function childLinks(x, i) {
  var myDiagram = window.myDiagram;
  
  
  var childLst = new go.List(go.Link);
  myDiagram.links.each(function (link) {
    if (link.containingGroup === x) {
      link.highlight = i;
      childLst.add(link);
    }
  });
  return childLst;
}

// same as childLinks, then run allMemberLinks for each child Group with the next color
function allMemberLinks(x, i) {
  var myDiagram = window.myDiagram;
  
  childLinks(x, i);
  myDiagram.nodes.each(function (node) {
    if (node instanceof go.Group && node.containingGroup === x) {
      allMemberLinks(node, i + 1);
    }
  });
}

// perform the actual highlighting
function highlight(shp, obj2, hl) {
  var color;
  var width = 3;
  if (hl === 0) {
    color = "black";
    width = 1;
  }
  else if (hl === 1) {
    color = "blue";
  }
  else if (hl === 2) {
    color = "green";
  }
  else if (hl === 3) {
    color = "orange";
  }
  else if (hl === 4) {
    color = "red";
  }
  else {
    color = "purple";
  }
  shp.stroke = color;
  shp.strokeWidth = width;
  if (obj2 !== null) {
    obj2.stroke = color;
    obj2.fill = color;
  }
}

// return the selected radio button in "highlight"
function getRadioButton() {
  var radio = document.getElementsByName("highlight");
  for (var i = 0; i < radio.length; i++)
    if (radio[i].checked) return radio[i];
}

// returns the text for a tooltip, param obj is the text itself
function getInfo(model, obj) {
  var x = obj.panel.adornedPart; // the object that the mouse is over
  var text = ""; // what will be displayed
  if (x instanceof go.Node) {
    if (x instanceof go.Group) text += "Group: "; else text += "Node: ";
    text += x.data.key;
    var toLst = nodesTo(x, 0); // display names of nodes going into this node
    if (toLst.count > 0) {
      toLst.sort(function (a, b) {
        return a > 0
      });
      
      //frLst.sort(function (a, b) {
      //  return a > 0
      //});
      
      
      text += "\nMember links: ";
      var linkStrings = new go.List("string");
      //linkChildren.each(function (link) {
      //  linkStrings.add(link.data.from + " --> " + link.data.to);
      //});
      linkStrings.sort(function (a, b) {
        return a > b
      });
      
      text += x.data.to + "\nNode To: " + x.data.to + "\nNode From: " + x.data.from;
      var grp = containing(x, 0); // and containing group, if it has one
      if (grp !== null) text += "\nContaining SubGraph: " + grp.data.key;
      
      return text;
    }
  }
}


// a conversion function used to get arrowhead information for a Part
function infoString(obj) {
  var part = obj.part;
  if (part instanceof go.Adornment) part = part.adornedPart;
  var msg = "";
  if (part instanceof go.Link) {
    var link = part;
    msg = "toArrow: " + link.data.toArrow + ";\nfromArrow: " + link.data.fromArrow;
  } else if (part instanceof go.Node) {
    var node = part;
    var link = node.linksConnected.first();
    msg = "toArrow: " + link.data.toArrow + ";\nfromArrow: " + link.data.fromArrow;
  }
  return msg;
}

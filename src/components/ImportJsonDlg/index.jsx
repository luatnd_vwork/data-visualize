import React from 'react'
import { Modal, Button } from 'antd';
import style from './style.module.scss'


export default class ImportJsonDlg extends React.Component {
  state = {visible: false}
  showModal = () => {
    this.setState({visible: true});
  }
  handleOk = (e) => {
    this.setState({visible: false});
  }
  handleCancel = (e) => {
    this.setState({visible: false});
  }
  
  render() {
    return (
      <div>
        <Button type="primary" icon="book" size={'large'} onClick={this.showModal}>Guide</Button>
        
        <Modal
          title="User guide"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          footer={<Button icon="smile-o" onClick={this.handleCancel}>Close</Button>}
        >
          <p>If you wanna update this EER data to newest version of [Migrate Logic] spreadsheet:</p>
          <ul>
            <li>Open [Migrate Logic] spreadsheet</li>
            <li>Choose <b>Export</b> --> <b>Sync data to EER UI</b></li>
            <li>Just wait for a tik tak</li>
            <li>Data was sync to this Web UI in real-time</li>
          </ul>
        </Modal>
      </div>
    );
  }
}
import React, { Component } from 'react';
import 'antd/dist/antd.css';  // or 'antd/dist/antd.less'
import emitter from './base/event/emitter'

import EER from './pages/EER';
//import EER2 from './pages/EER/index2';
import ImportJsonDlg from './components/ImportJsonDlg';

import style from './App.module.scss';

import { Layout, Menu, Breadcrumb, Switch, Popover } from 'antd';
import { Row, Col } from 'antd';
import { Button } from 'antd';

const { Header, Content } = Layout;


class App extends Component {
  render() {
    return (
      <div className="App">
        
        <Layout className={style.layout}>
          <Header className={style.header}>
            
            <Row className={style.headerRow}>
              <Col span={12}>
                <Menu
                  theme="dark"
                  mode="horizontal"
                  defaultSelectedKeys={['1']}
                  style={{ lineHeight: '64px' }}
                >
                  <Menu.Item key="1">EER</Menu.Item>
                  <Menu.Item key="2">PERT</Menu.Item>
                </Menu>
              </Col>
              
              <Col span={12}>
                <div className={style.toolbar}>
                  <ImportJsonDlg/>
                </div>
              </Col>
              
            </Row>
          </Header>
          
          <Content className={style.antContent}>
            
            <div className={style.breadCrum}>
              <Breadcrumb>
                <Breadcrumb.Item>EER Diagram</Breadcrumb.Item>
              </Breadcrumb>
              
              <div className={style.breadToolbar}>
                <Popover content={<p className={style.tooltip}><b>Show</b> or <b>hide</b> tables's detail content</p>} trigger="hover" placement="left">
                  <span className={style.breadToolbarText}>Collapse</span>
                  <Switch defaultChecked onChange={checked => emitter.emit('EER_updateCollapseState', checked)}/>
                </Popover>
              </div>
            </div>
            
            <div className={style.contentC}>
              
              <EER/>
              {/*<EER2/>*/}
              
            </div>
            
          </Content>
        </Layout>

      </div>
    );
  }
}

export default App;

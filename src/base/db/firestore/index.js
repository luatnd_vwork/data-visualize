import firebase from "firebase";
import "firebase/firestore";

import emitter from '../../event/emitter'


export default class FireStore {
  constructor() {
    this.init();
  }
  
  db = null;
  
  init() {
    console.log("initial");
    
    firebase.initializeApp({
      apiKey: "AIzaSyCX_TlVLtG8b0Tj-c3WZ-JqysMaocXnZFg",
      authDomain: "fire-visualize.firebaseapp.com",
      projectId: "fire-visualize",
    });
    
    
    // Initialize Cloud Firestore through Firebase
    this.db = firebase.firestore();
  }
  
  unSubscribeEerDoc = () => null;
  
  subscribeEerDoc() {
    this.unSubscribeEerDoc = this.db.collection("eer").doc("exportedData")
      .onSnapshot(function (doc) {
        const source = doc.metadata.hasPendingWrites ? "Local" : "Server";
        const newData = doc.data();
        
        console.log('Found new data from: ' + source);
        
        if (source === 'Server') {
          emitter.emit('onEerNewData', newData);  // Listener prints "5 10".
        }
      });
  }
  
  unsubscribeEerDoc() {
    this.unSubscribeEerDoc();
  }
}